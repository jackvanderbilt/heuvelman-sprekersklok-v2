module.exports = {
    root: true,
    env: {
        browser: true,
        node: true
    },
    parserOptions: {
        parser: 'babel-eslint'
    },
    extends: [
        '@nuxtjs',
        'plugin:nuxt/recommended'
    ],
    // add your custom rules here
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',

        'brace-style': [ 'error', 'allman' ],
        curly: [ 'error', 'multi-line' ],
        'vue/html-indent': [ 'error', 4 ],
        'vue/max-attributes-per-line': [
            'error',
            {
                singleline: 2,
                multiline: {
                    max: 1,
                    allowFirstLine: false
                }
            }
        ],
        'vue/singleline-html-element-content-newline': [
            'error',
            {
                ignoreWhenNoAttributes: true,
                ignoreWhenEmpty: true,
                ignores: [
                    'pre',
                    'textarea'
                ]
            }
        ],
        'vue/html-self-closing': [ 'error', {
            html: {
                void: 'always',
                normal: 'never',
                component: 'never'
            }
        }],
        'max-len': [ 0 ],
        'space-in-parens': [ 'error', 'always', { exceptions: [ '{}' ] }],
        'array-bracket-spacing': [ 'error', 'always', { objectsInArrays: false }],
        semi: [ 'error', 'never' ],
        'comma-dangle': [ 'error', 'never' ],
        'quote-props': [ 'error', 'as-needed' ],
        'spaced-comment': [ 'error', 'always', { markers: [ '?', 'TODO', '!', '*' ] }],
        indent: [ 'error', 4 ],
        'class-methods-use-this': 'off',
        'lines-between-class-members': [ 'error', 'always', { exceptAfterSingleLine: true }]
    }
}
