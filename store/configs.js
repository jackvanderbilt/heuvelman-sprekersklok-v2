export default {
    getters: {
        configs ()
        {
            return require.context( '~/configs/', true, /.json$/ ).keys()
                .map(
                    config => ({
                        name: config?.split( '/' )?.[1].slice( 0, -5 ),
                        dir: `~/configs/${config?.slice( 2, config.length )}`
                    })
                )
                .filter( ({ name, dir }) => name && dir )
        }
    }
}
