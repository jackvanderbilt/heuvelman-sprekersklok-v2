# heuvelman-sprekersklok-v2

# Requirements
- NodeJS v1.12 of een hogere recommended build. Te vinden op https://nodejs.org/en/download

# Configuratie toevoegen
Dupliceer een bestaande configuratie in en naar de `/configs` folder. Bijvoorbeeld door een config te kopieren en plakken.
Geef de config een duidelijke naam. De naam van het bestand bepaald hoe de config op de landingspagina word weergegeven in de lijst.

# Logos
Logo's moet je plaatsen in de `/logos` folder. Deze kan je vervolgens in de configs refereren.
Voeg geen extra `/` of iets dergelijks toe aan de logo URL in de config. Wel altijd de juiste bestands type (.jpg, .png, etc) toevoegen. 
De layout maakt de logo's automatisch het beste zichtbaar. Zo niet kan je de styling aanpassen in `/pages/_config/*` folder.

# Getting started

1. Install dependencies met het volgende commando:

`$ npm install`



2. Start dev server met het volgende commando:

`$ npm run dev`

Nu kan je wijzigingen maken. Deze worden automatisch weergegeven zonder te refreshen wanneer je een bestand opslaat, verwijderd of toevoegfd

# EXPORTEREN

0. Indien nog niet gedaan: install dependencies met het volgende commando: `$ npm install` 
1. Testen of het werkt: `$ npm run dev`
2. Gereed voor implementatie? build for production:
`$ npm run build`

3. Op locatie moet je dan de bestanden kopieren op de server in die folder waar je de bestanden naartoe hebt gekopieerd het volgende commando uitvoeren om de server te starten:
`$ npm run start`

4. Wanneer server is gestart, open de link die in je console komt.
enjoy a working sprekersklok 💪