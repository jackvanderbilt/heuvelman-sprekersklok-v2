import http from 'http'
import socketIO from 'socket.io'

export default {
    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        title: 'sprekersklok-v2',
        htmlAttrs: {
            lang: 'nl'
        },
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: '' }
        ]
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [
    ],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
    ],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [
    // https://go.nuxtjs.dev/eslint
        '@nuxtjs/eslint-module',
        // https://go.nuxtjs.dev/tailwindcss
        '@nuxtjs/tailwindcss'
    ],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
        function ()
        {
            this.nuxt.hook( 'render:before', ( renderer ) =>
            {
                const server = http.createServer( this.nuxt.renderer.app )
                const io = socketIO( server )

                // overwrite nuxt.server.listen()
                this.nuxt.server.listen = ( port, host ) => new Promise( resolve => server.listen( port || 3000, host || 'localhost', resolve ) )
                // close this server on 'close' event
                this.nuxt.hook( 'close', () => new Promise( server.close ) )

                // Add socket.io events
                let clockConfig = null

                io.on( 'connection', ( socket ) =>
                {
                    // Send new config to the client (just in case it is a /view (viewer))
                    socket.emit( 'new-config', clockConfig )
                    socket.on( 'update-config', function ( newClockConfig )
                    {
                        clockConfig = newClockConfig // Cache config

                        // Send new config to all clients
                        socket.broadcast.emit( 'new-config', clockConfig )
                        console.log( 'received new config. Sending to viewers...' )
                    })
                })
            })
        }
    ],

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {
    }
}
