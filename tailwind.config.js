module.exports = {
    theme: {},
    variants: [ 'responsive', 'hover', 'focus', 'active', 'group-hover', 'disabled' ],
    plugins: [],
    purge: {
        enabled: process.env.NODE_ENV === 'production',
        content: [
            'pages/**/*.vue',
            'nuxt.config.js'
        ]
    }
}
